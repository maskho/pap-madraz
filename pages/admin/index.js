import { Row, Col, Card, CardBody, CardTitle, CardFooter } from "reactstrap";
import Link from "next/link";

export default function Dashboard() {
  return (
    <>
      <div className="content">
        <Row>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-globe text-warning" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Kelas</p>
                      <CardTitle tag="p">X MIPA 2</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Row>
                  <Col md="4" xs="5">
                    <div className="stats">
                      <i className="nc-icon nc-chart-bar-32" />
                      Statistik
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="stats text-right">
                      <i className="nc-icon nc-single-02" />
                      25 Murid
                    </div>
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-favourite-28 text-primary" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Kelas</p>
                      <CardTitle tag="p">X MIPA 4</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Row>
                  <Col md="4" xs="5">
                    <div className="stats">
                      <i className="nc-icon nc-chart-bar-32" />
                      Statistik
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="stats text-right">
                      <i className="nc-icon nc-single-02" />
                      35 Murid
                    </div>
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
          <Col lg="3" md="6" sm="6">
            <Card className="card-stats">
              <CardBody>
                <Row>
                  <Col md="4" xs="5">
                    <div className="icon-big text-center icon-warning">
                      <i className="nc-icon nc-vector text-danger" />
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="numbers">
                      <p className="card-category">Kelas</p>
                      <CardTitle tag="p">XI IPS</CardTitle>
                      <p />
                    </div>
                  </Col>
                </Row>
              </CardBody>
              <CardFooter>
                <hr />
                <Row>
                  <Col md="4" xs="5">
                    <div className="stats">
                      <i className="nc-icon nc-chart-bar-32" />
                      Statistik
                    </div>
                  </Col>
                  <Col md="8" xs="7">
                    <div className="stats text-right">
                      <i className="nc-icon nc-single-02" />
                      32 Murid
                    </div>
                  </Col>
                </Row>
              </CardFooter>
            </Card>
          </Col>
        </Row>
        <Link href="/">
          <a> kembali</a>
        </Link>
      </div>
    </>
  );
}
