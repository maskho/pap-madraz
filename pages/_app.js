import "../assets/css/bootstrap.min.css";
import "../assets/css/paper-dashboard.css";
import "../assets/css/paper-kit.css";
import "../assets/demo/demo.css";

function MyApp({ Component, pageProps }) {
  return <Component {...pageProps} />;
}
export default MyApp;
