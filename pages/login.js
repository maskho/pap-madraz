import LandingNavbar from "../components/Navbar";
import { Button, Card, Form, Input, Container, Row, Col } from "reactstrap";

export default function Login() {
  if (typeof document !== "undefined") {
    document.documentElement.classList.remove("nav-open");
    React.useEffect(() => {
      document.body.classList.add("register-page");
      return function cleanup() {
        document.body.classList.remove("register-page");
      };
    });
  }
  return (
    <>
      <LandingNavbar />
      <div
        className="page-header"
        style={{
          backgroundImage:
            "url(" + require("../assets/img/login-image.jpg") + ")",
        }}
      >
        <div className="filter" />
        <Container>
          <Row>
            <Col className="ml-auto mr-auto" lg="4">
              <Card className="card-register ml-auto mr-auto">
                <h3 className="title mx-auto">
                  Hai! Semoga Harimu Menyenangkan!
                </h3>

                <Form className="register-form">
                  <label>Email</label>
                  <Input placeholder="Masukkan email" type="text" />
                  <label>Password</label>
                  <Input placeholder="Masukkan password" type="password" />
                  <Button
                    block
                    className="btn-round"
                    color="danger"
                    href="/admin"
                  >
                    Register
                  </Button>
                </Form>
                <div className="forgot">
                  <Button
                    className="btn-link"
                    color="danger"
                    href="/admin"
                    onClick={(e) => e.preventDefault()}
                  >
                    Lupa password?
                  </Button>
                </div>
              </Card>
            </Col>
          </Row>
        </Container>
        <div className="footer register-footer text-center">
          <h6>
            © {new Date().getFullYear()}, dibuat dengan{" "}
            <i className="fa fa-heart heart" /> oleh Madraz Band
          </h6>
        </div>
      </div>
    </>
  );
}
